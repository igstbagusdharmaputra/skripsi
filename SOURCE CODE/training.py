import pandas as pd
from sklearn.model_selection import KFold

def counter(data,colname,label,target):
    temp = (data[colname] == label) & (data['class'] == target)
    return len(data[temp])

total = 0
print('waiting....')
print('Proses Training dengan 20 fitur')
#seleksi fitur
kolom = ['service','flag','same_srv_rate','dst_host_srv_count','logged_in','dst_host_same_srv_rate','dst_host_srv_serror_rate','srv_serror_rate','serror_rate','dst_host_serror_rate',
        'count','dst_host_count','protocol_type','dst_host_srv_rerror_rate','rerror_rate','dst_host_rerror_rate','srv_rerror_rate','srv_count','dst_host_srv_diff_host_rate','dst_host_diff_srv_rate','class']
#41 atribut
# kolom = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
#              'urgent', 'hot', 'num_failed_logins',
#              'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations',
#              'num_shells', 'num_access_files', 'num_outbound_cmds',
#              'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
#              'srv_rerror_rate', 'same_srv_rate',
#              'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate',
#              'dst_host_diff_srv_rate',
#              'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate',
#              'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
#              'dst_host_srv_rerror_rate', 'class']

data = pd.read_csv('resulttrain.csv',usecols=kolom)
X = data.iloc[:,:-1]
k = 10
kf = KFold(n_splits=k, random_state=None)
index = 0
# split_data = [65,70,80,90]
for train_index , test_index in kf.split(X):
    prediksi = [] #menyimpan hasil prediksi nilai
    probabilitas = {0:{},1:{}} #menyimpan nilai probabilitas
    #cross val
    train_X = data.iloc[train_index,:] #mengambil data sebanyak train_len
    test_X = data.iloc[test_index,:-1] #mendapatkan nilai data test
    test_Y = data.iloc[test_index,-1] #mengambil nilai class

    #data,colname,label,target
    count_yes = counter(train_X,'class','normal','normal') #total  class yes pada data
    count_no = counter(train_X, 'class','anomaly','anomaly') #total class no pada data
    #count_neptune = counter(train_X, 'class', 'neptune', 'neptune')
    prob_yes = count_yes/len(train_X) #nilai probabilitas pada class yes
    prob_no = count_no/len(train_X) #nilai probabilitas pada class no
    #training model
    for j in train_X: #train_X.columns[:-1]
        # nilai probabilitas pada setiap atribut terhadap class
        probabilitas[1][j] = {}
        probabilitas[0][j] = {}
        #probabilitas[2][j] = {}
        for k in train_X[j].unique():
            count_k_yes = counter(train_X, j, k, 'normal')
            count_k_no = counter(train_X, j, k, 'anomaly')
            probabilitas[1][j][k] = count_k_yes/count_yes
            probabilitas[0][j][k] = count_k_no/count_no
    #test model
    for baris in range(0,len(test_X)):
        hasil_yes = prob_yes
        hasil_no = prob_no
        for kolom in test_X.columns:
            try:
                hasil_yes *= probabilitas[1][kolom][test_X[kolom].iloc[baris]]
                hasil_no  *= probabilitas[0][kolom][test_X[kolom].iloc[baris]]
            except:
                continue
        if hasil_yes > hasil_no :
            prediksi.append('normal')
        else:
            prediksi.append('anomaly')
    #confusion matrix
    tp, tn, fp, fn = 0, 0, 0, 0
    for j in range(0,len(prediksi)):
        if prediksi[j] == 'normal':
            if test_Y.iloc[j] == 'normal':
                tp+=1
            else:
                fp+=1
        else:
            if test_Y.iloc[j] == 'anomaly':
                tn+=1
            else:
                fn+=1
    index = index+1
    temp = (tp+tn)/len(test_Y)
    total+=temp
    print('Akurasi dengan data training fold '+str(index)+ ' :' ,(tp+tn)/len(test_Y)*100,'%')


print('Hasil Akurasi',(total/10)*100,'%')