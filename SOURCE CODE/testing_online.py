import pandas as pd
from sklearn.model_selection import KFold


def counter(data,colname,label,target):
    temp = (data[colname] == label) & (data['class'] == target)
    return len(data[temp])


print('waiting....')

kolom = ['service','flag','same_srv_rate','dst_host_srv_count','logged_in','dst_host_same_srv_rate','dst_host_srv_serror_rate','srv_serror_rate','serror_rate','dst_host_serror_rate',
            'count','dst_host_count','protocol_type','dst_host_srv_rerror_rate','rerror_rate','dst_host_rerror_rate','srv_rerror_rate','srv_count','dst_host_srv_diff_host_rate','dst_host_diff_srv_rate','class']

kolomtest = ['service','flag','same_srv_rate','dst_host_srv_count','logged_in','dst_host_same_srv_rate','dst_host_srv_serror_rate','srv_serror_rate','serror_rate','dst_host_serror_rate',
            'count','dst_host_count','protocol_type','dst_host_srv_rerror_rate','rerror_rate','dst_host_rerror_rate','srv_rerror_rate','srv_count','dst_host_srv_diff_host_rate','dst_host_diff_srv_rate','class']


# kolom = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
#              'urgent', 'hot', 'num_failed_logins',
#              'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations',
#              'num_shells', 'num_access_files', 'num_outbound_cmds',
#              'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
#              'srv_rerror_rate', 'same_srv_rate',
#              'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate',
#              'dst_host_diff_srv_rate',
#              'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate',
#              'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
#              'dst_host_srv_rerror_rate', 'class']
#
# kolomtest = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
#              'urgent', 'hot', 'num_failed_logins',
#              'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations',
#              'num_shells', 'num_access_files', 'num_outbound_cmds',
#              'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
#              'srv_rerror_rate', 'same_srv_rate',
#              'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate',
#              'dst_host_diff_srv_rate',
#              'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate',
#              'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
#              'dst_host_srv_rerror_rate', 'class']
data = pd.read_csv('resulttrain.csv',usecols=kolom)
datacoba = pd.read_csv('resulttest_online.csv',usecols=kolomtest)
test_Y = datacoba.iloc[:,-1]
datacoba = datacoba.iloc[:,:-1]
total = 0
#------
X = data.iloc[:,:-1]
k = 10
kf = KFold(n_splits=k,random_state=None)
index = 0
#-----

#split_data = [70]
#for i in split_data:
for train_index,test in kf.split(X):
#    print(train_index)
    prediksi = [] #menyimpan hasil prediksi nilai
    probabilitas = {0:{},1:{}} #menyimpan nilai probabilitas
    #train_len = int((i*len(data))/100) #menghitung banyaknya nilai training
    #split training dan testing
 #   train_X = data.iloc[:train_len,:] #mengambil data sebanyak train_len
    train_X = data.iloc[train_index, :]
    # print(test)
    #data,colname,label,target
    count_yes = counter(train_X,'class','normal','normal') #total  class yes pada data
    count_no = counter(train_X, 'class','anomaly','anomaly') #total class no pada data
    prob_yes = count_yes/len(train_X) #nilai probabilitas pada class yes
    prob_no = count_no/len(train_X) #nilai probabilitas pada class no
    #training model
    for j in  train_X: #train_X.columns[:-1]:
        # nilai probabilitas pada setiap atribut terhadap class
        probabilitas[1][j] = {}
        probabilitas[0][j] = {}
        for k in train_X[j].unique():
            count_k_yes = counter(train_X, j, k, 'normal')
            count_k_no = counter(train_X, j, k, 'anomaly')
            probabilitas[1][j][k] = count_k_yes/count_yes
            probabilitas[0][j][k] = count_k_no/count_no
    #test model
    for baris in range(0,len(datacoba)):
        hasil_yes = prob_yes
        hasil_no = prob_no
        for kolom in datacoba.columns:
            try:
                hasil_yes *= probabilitas[1][kolom][datacoba[kolom].iloc[baris]]
                hasil_no  *= probabilitas[0][kolom][datacoba[kolom].iloc[baris]]
            except:
                continue
        if hasil_yes > hasil_no:
            prediksi.append('normal')
        else:
            prediksi.append('anomaly')

        # confusion matrix
        tp, tn, fp, fn = 0, 0, 0, 0
        for j in range(0, len(prediksi)):
            if prediksi[j] == 'normal':
                if test_Y.iloc[j] == 'normal':
                    tp += 1
                    #print('predicted = ' + str(prediksi[j]) + ' actual= ' + str(test_Y.iloc[j]) + '' + '=> Sama')
                else:
                    fp += 1
                    #print('predicted = ' + str(prediksi[j]) + ' actual= ' + str(test_Y.iloc[j]) + '' + '=> Tidak Sama')
            else:
                if test_Y.iloc[j] == 'anomaly':
                    tn += 1
                    #print('predicted = ' + str(prediksi[j]) + ' actual= ' + str(test_Y.iloc[j]) + '' + '=> Sama')
                else:
                    fn += 1
                    #print('predicted = ' + str(prediksi[j]) + ' actual= ' + str(test_Y.iloc[j]) + '' + '=> Tidak Sama')

    index = index+1
    temp = (tp+tn)/len(test_Y)
    total+=temp
    print((tp+tn),'/',len(test_Y))
    print('Akurasi dengan data training fold '+str(index)+ ' :' ,(tp+tn)/len(test_Y)*100,'%')
    #print('Hasil Akurasi dengan data training ' + str(train_len) + ' :', (tp + tn) / len(test_Y) * 100, '%')

print('Hasil Akurasi',(total/10)*100,'%')


#1120 baris untuk data wireshark